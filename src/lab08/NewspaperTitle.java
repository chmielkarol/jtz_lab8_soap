package lab08;

import java.io.Serializable;

public class NewspaperTitle implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5849753695354247012L;
	private String title;
	private int amount;
	
	NewspaperTitle(){
		title = "Gazeta";
		amount = 1;
	}
	
	NewspaperTitle(NewspaperTitle nt){
		title = nt.title;
		amount = nt.amount;
	}

	NewspaperTitle(String title, int amount){
		this.title = title;
		this.amount = amount;
	}
	
	int getAmount() {
		return amount;
	}
	
	void setAmount(int amount) {
		this.amount = amount;
	}
	
	String getTitle() {
		return title;
	}
	
	String[] getInfoRow() {
		String[] row = {title, Integer.toString(amount)};
		return row;
	}
}
