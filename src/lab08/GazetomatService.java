package lab08;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface GazetomatService {
	
	@WebMethod
	public int getAmount();
	
	@WebMethod
	public int getTitleAmount(int index);
	
	@WebMethod
	public String getTitle(int index);
}