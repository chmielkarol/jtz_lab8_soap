package lab08;

import java.awt.EventQueue;


import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Service;
import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.net.URL;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Gazetomat {

	private JFrame frmGazetomat;
	private JTable table;
	
	private DefaultTableModel model;
	private JScrollPane scrollPane;
	
	private static GazetomatServiceImpl gsi;
	
	private static int ID;
	private static ManagerService ms;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					gsi = new GazetomatServiceImpl();
					gsi.add(new NewspaperTitle("Rzeczpospolita", 3));
					gsi.add(new NewspaperTitle("Teletydzie�", 1));
					gsi.add(new NewspaperTitle("Echo Dnia", 2));
					gsi.add(new NewspaperTitle("Gazeta wroc�awska", 3));
					
					URL wsdlURL = new URL("http://localhost:9000/manager?wsdl");
					
					//creating QName using targetNamespace and name
					QName qname = new QName("http://lab08/", "ManagerServiceImplService"); 
					
					Service service = Service.create(wsdlURL, qname);  
					
					//We need to pass interface and model beans to client
					ms = service.getPort(ManagerService.class);
					
					ID = ms.register();
					
					int port = 8000 + ID;
					String myURL = "http://localhost:" + port + "/gazetomat";
					Endpoint.publish(myURL, gsi);
					
					Gazetomat window = new Gazetomat();
					window.frmGazetomat.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Gazetomat() {
		initialize();
		
		refresh();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGazetomat = new JFrame();
		frmGazetomat.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				ms.unregister(ID);
			}
		});
		frmGazetomat.setTitle("Gazetomat " + ID);
		frmGazetomat.setBounds(100, 100, 450, 300);
		frmGazetomat.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGazetomat.getContentPane().setLayout(null);
		
		model = new DefaultTableModel();
		
		JButton btnBuy = new JButton("KUP");
		btnBuy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int index = table.getSelectedRow();
				if(index >= 0) {
					String selectedTitle = (String) model.getValueAt(index, 0);
					gsi.buy(selectedTitle);
					
					refresh();
				}
			}
		});
		btnBuy.setBounds(366, 207, 58, 43);
		frmGazetomat.getContentPane().add(btnBuy);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 11, 346, 239);
		frmGazetomat.getContentPane().add(scrollPane);
		table = new JTable(model);
		table.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				if(table.getSelectedRow() == -1)
					btnBuy.setEnabled(false);
				else
					btnBuy.setEnabled(true);
			}
			@Override
			public void focusLost(FocusEvent arg0) {
				if(table.getSelectedRow() == -1)
					btnBuy.setEnabled(false);
				else
					btnBuy.setEnabled(true);
			}
		});
		scrollPane.setViewportView(table);
		table.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}
	
	private void refresh() {
		model = new DefaultTableModel();
		model.addColumn("Tytu�");
		model.addColumn("Zosta�o");
		
		table.setModel(model);
		NewspaperTitle[] newspaperTitles = gsi.getNewspaperTitles();
		for (NewspaperTitle newspaperTitle : newspaperTitles) {
			String[] infoRow = newspaperTitle.getInfoRow();
			model.addRow(infoRow);
		}
	}
}
