package lab08;

import java.awt.EventQueue;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.BevelBorder;
import javax.swing.table.DefaultTableModel;
import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Service;

import java.awt.BorderLayout;
import javax.swing.JTabbedPane;
import javax.swing.JPanel;

public class Manager {

	private JFrame frmManader;
	
	private static JTabbedPane tabbedPane;
	
	private static ManagerServiceImpl msi;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					msi = new ManagerServiceImpl();
					Manager window = new Manager();
					window.frmManader.setVisible(true);
					
					refresh();
					
					Endpoint.publish("http://localhost:9000/manager", msi);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Manager() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmManader = new JFrame();
		frmManader.setTitle("Menad\u017Cer");
		frmManader.setBounds(100, 100, 450, 300);
		frmManader.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frmManader.getContentPane().setLayout(new BorderLayout(0, 0));
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frmManader.getContentPane().add(tabbedPane, BorderLayout.CENTER);
	}
	
	private static void refresh() {
		Thread thread = new Thread(new Runnable() {
			public void run() {
				try {
					while(true) {
						Thread.sleep(5000);
						
						tabbedPane.removeAll();
						
						List<Integer> gazetomatIDs = msi.getGazetomatIDs();
						for(Integer ID : gazetomatIDs) {
							int port = 8000 + ID;
							String url = "http://localhost:" + port + "/gazetomat?wsdl";
							URL wsdlURL = new URL(url);
							
							//creating QName using targetNamespace and name
							QName qname = new QName("http://lab08/", "GazetomatServiceImplService"); 
							
							Service service = Service.create(wsdlURL, qname);  
							
							//We need to pass interface and model beans to client
							GazetomatService gs = service.getPort(GazetomatService.class);
	
							JPanel panel = new JPanel();
							tabbedPane.addTab("Gazetomat " + ID, null, panel, null);
	
							DefaultTableModel model = new DefaultTableModel();
							model.addColumn("Tytu�");
							model.addColumn("Zosta�o");
	
							int amount = gs.getAmount();
							for(int i = 0; i < amount; ++i) {
								int titleAmount = gs.getTitleAmount(i);
								String title = gs.getTitle(i);
								String[] row = new String[] {title, Integer.toString(titleAmount)};
								model.addRow(row);
							}
							
							JScrollPane scrollPane = new JScrollPane();
							panel.add(scrollPane);
							
							JTable table = new JTable(model);
							scrollPane.setViewportView(table);
							table.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
						}
	
					}
					
				} catch (InterruptedException | MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		thread.start();
	}
}
