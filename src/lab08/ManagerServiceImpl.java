package lab08;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

@WebService(endpointInterface = "lab08.ManagerService")
public class ManagerServiceImpl implements ManagerService {
	
	private static int lastID;
	
	private List<Integer> gazetomatIDs;
	
	ManagerServiceImpl(){
		lastID = -1;
		gazetomatIDs = new ArrayList<Integer>();
	}

	@Override
	public int register() {
		++lastID;
		gazetomatIDs.add(lastID);
		return lastID;
	}

	@Override
	public void unregister(int ID) {
		gazetomatIDs.remove(Integer.valueOf(ID));
	}
	
	public List<Integer> getGazetomatIDs(){
		return gazetomatIDs;
	}

}
