package lab08;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

@WebService(endpointInterface = "lab08.GazetomatService")
public class GazetomatServiceImpl implements GazetomatService {
	
	private static List<NewspaperTitle> newspaperTitles;
	
	GazetomatServiceImpl(){
		newspaperTitles = new ArrayList<NewspaperTitle>();
	}
	
	public void buy(String selectedTitle) {
		int size = newspaperTitles.size();
		for(int i = 0; i < size; ++i) {
			if(newspaperTitles.get(i).getTitle().equals(selectedTitle)) {
				int amount = newspaperTitles.get(i).getAmount();
				if(amount == 1) {
					newspaperTitles.remove(newspaperTitles.get(i));
					return;
				}
				else {
					--amount;
					newspaperTitles.get(i).setAmount(amount);
					return;
				}
			}
		}
	}
	
	public void add(NewspaperTitle nt) {
		newspaperTitles.add(nt);
	}

	public NewspaperTitle[] getNewspaperTitles() {
		int size = newspaperTitles.size();
		NewspaperTitle[] array = new NewspaperTitle[size];
		
		for(int i = 0; i < size; ++i) {
			array[i] = new NewspaperTitle(newspaperTitles.get(i).getTitle(), newspaperTitles.get(i).getAmount());
		}
		return array;
	}

	@Override
	public int getAmount() {
		return newspaperTitles.size();
	}

	@Override
	public int getTitleAmount(int index) {
		int size = newspaperTitles.size();
		if(index < size) {
			return newspaperTitles.get(index).getAmount();
		}
		return 0;
	}

	@Override
	public String getTitle(int index) {
		int size = newspaperTitles.size();
		if(index < size) {
			return newspaperTitles.get(index).getTitle();
		}
		return "Gazeta";
	}
}
