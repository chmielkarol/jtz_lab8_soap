package lab08;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface ManagerService {
	
	@WebMethod
	public int register();
	
	@WebMethod
	public void unregister(int ID);
}